//Syntax: fetch(url, options)
fetch('https://jsonplaceholder.typicode.com/posts').then(response => response.json()).then((data) => showPosts(data))

//Add Post
document.querySelector('#form-add-post').addEventListener('submit', (e) => {
	e.preventDefault();
	fetch('https://jsonplaceholder.typicode.com/posts', {
		method: 'POST',
		body: JSON.stringify({
			title: document.querySelector('#txt-title').value,
			body: document.querySelector('#txt-body').value,
			userId: 1
		}),
		headers: {
			'Content-Type': 'application/json'
		}
	})
	.then(response => response.json())
	.then(data => {
		console.log(data)
		alert(`Post Successfully Added!`)
		document.querySelector('#txt-title').value = null
		document.querySelector('#txt-body').value = null
	})
})

//update post 
document.querySelector('#form-edit-post').addEventListener('submit', (e) => {
	e.preventDefault()
	fetch('https://jsonplaceholder.typicode.com/posts/1',{
		method: 'PUT',
		body: JSON.stringify({
			id: document.querySelector('#txt-edit-id').value,
			title: document.querySelector('#txt-edit-title').value,
			body: document.querySelector('#txt-edit-body').value,
			userId: 1
		}),
		headers: {
			'Content-Type': 'application/json'
		}
	})
	.then(response => response.json())
	.then(data => {
		console.log(data)
		alert(`Post Successfully Edited`)
		document.querySelector('#txt-title').value = null
		document.querySelector('#txt-body').value = null
		document.querySelector('#btn-submit-update').setAttribute('disabled', true)
	})
})
//Show post
const showPosts = (posts) => {
	let postEntries = ''

	posts.forEach(post => {
		postEntries += `
			<div id="post-${post.id}">
				<h3 id="post-title-${post.id}">${post.title}</h3>
				<p id="post-body-${post.id}">${post.body}</p>
				<button class="btn btn-warning btn-sm" onClick="editPost('${post.id}')">Edit</button>
				<button class="btn btn-danger btn-sm" onClick="deletePost('${post.id}')">Delete</button>
			</div> 
			`;
	})
	document.querySelector('#post-entries').innerHTML = postEntries;
}


//Edit Post
const editPost = (id) => {
	let title = document.querySelector(`#post-title-${id}`).innerHTML
	let body = document.querySelector(`#post-body-${id}`).innerHTML
	
	document.querySelector(`#txt-edit-id`).value = id
	document.querySelector(`#txt-edit-title`).value = title
	document.querySelector(`#txt-edit-body`).value = body
	document.querySelector('#btn-submit-update').removeAttribute('disabled')
}

const deletePost = (postId) => {
	fetch('https://jsonplaceholder.typicode.com/posts')
	.then(response => response.json())
	.then(data => {
		let index = data.findIndex(post => post.id === parseInt(postId));
		if(index > -1){
			data.splice(index, 1)
			alert(`Successfully Deleted Post`);
		}
		showPosts(data)
	}) 
};
